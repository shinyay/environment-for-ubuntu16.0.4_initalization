" ---------- dein.vim configuration ---------
" ----- Directory settings -----

if &compatible
  set nocompatible
endif

let g:rc_dir = expand('~/.vim')
let s:dein_dir = expand('~/.vim/dein')
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

" ----- Plugin settings -----
if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  " --- Plugin list path ---
  let s:toml      = g:rc_dir . '/rc/dein.toml'
  let s:lazy_toml = g:rc_dir . '/rc/dein_lazy.toml'

  " --- Plugin load ---
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})
  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif

filetype plugin on

" ----- Basic settings -----
syntax enable
set number
set showmatch
set backspace=indent,eol,start

" ----- Tab settings -----
set expandtab
set tabstop=8
set softtabstop=4

" ----- Indent settings -----
set autoindent
set shiftwidth=4

