#!/bin/bash
apt-get install -y vim
git config --global user.name "Shinya Yanagihara"
git config --global user.email "shinya.com@gmail.com"
git config --global core.editor 'vim -c "set fenc=utf-8"'
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

wget https://packages.chef.io/stable/ubuntu/12.04/chefdk_0.14.25-1_amd64.deb
dpkg -i chefdk_0.14.25-1_amd64.deb

chef verify

echo 'eval "$(chef shell-init bash)"' >> ~/.bash_profile
echo 'export EDITOR=vim' >> ~/.bashrc

